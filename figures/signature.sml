signature SYNTHESIS_PROBLEM = sig
  (* input alphabet *)
  type X

  (* output alphabet *)
  eqtype Y

  (* substrings *)
  type input_substr = X list
  type output_substr = Y list
  val toString : input_substr -> string

  (* useful functions *)
  type function = input_substr list -> output_substr
  val functionArities : int list
  val functionsOfArity : int -> function list

  (* character generalization hierarchy *)
  type G
  val generalize : X -> G
  val similarity : G * G -> real
  val combine    : G * G -> G
  val matchChar  : G * X -> bool
end
