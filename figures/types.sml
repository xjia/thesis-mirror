type correspondence =
  (P.input_substr * int) list * (P.output_substr * int)

type poconj = P.G list * P.G list

fun parser input =
  let fun parse [] _ = []
        | parse _ [] = []
        | parse (c::cs) input =
            let val pos = matchPattern c input
                val tok = List.take(input,pos)
                val rest = List.drop(input,pos)
            in  tok::parse cs rest
            end
  in  parse conjectures input
  end
