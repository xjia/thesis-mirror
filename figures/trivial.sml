structure TrivialSynthesisProblem : SYNTHESIS_PROBLEM = struct
  (* input alphabet *)
  type X = char

  (* output alphabet *)
  type Y = char

  (* substrings *)
  type input_substr = char list
  type output_substr = char list
  fun toString x = implode x

  (* useful functions *)
  type function = input_substr list -> output_substr
  val functionArities = [1]
  fun functionsOfArity k =
    case k of
         1 => [fn [x]=>x | _=>raise Match]
       | _ => raise Match

  (* character generalization hierarchy *)
  datatype G = Char of X | Any
  fun generalize x = Char x
  fun similarity (Char x, Char y) = if x=y then 1.0 else 0.0
    | similarity (Char _, Any)    = 0.5
    | similarity (Any, Char _)    = 0.5
    | similarity (Any, Any)       = 1.0
  fun combine (Char x, Char y) = if x=y then Char x else Any
    | combine (Char _, Any)    = Any
    | combine (Any, Char _)    = Any
    | combine (Any, Any)       = Any
  fun matchChar (Char x, y) = x=y
    | matchChar (Any, _)    = true
end

structure TrivialSynthesizer = SynthesizerFn(TrivialSynthesisProblem)

