fun pairwiseLCS ([], _) = []
  | pairwiseLCS (_, []) = []
  | pairwiseLCS (xs,ys) =
  let
    fun longest (x,y) = if length x > length y then x else y
    (* lastrow: a b *)
    (* cur row: c ? *)
    fun row (_, [], _, currow) = rev currow
      | row (x, y::ys, a::b::lastrow, c::currow) =
          if x=y then row(x, ys, b::lastrow, (x::a)::c::currow)
                 else row(x, ys, b::lastrow, longest(b,c)::c::currow)

    fun lcs ([], _, last)     = rev (hd (rev last))
      | lcs (x::xs, ys, last) = lcs(xs, ys, row(x, ys, last, [[]]))

    fun array (1, x) = [x]
      | array (n, x) = x::array(n-1, x)
  in
    lcs (xs, ys, array (length ys + 1, []))
  end
