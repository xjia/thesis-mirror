#!/bin/sh
pdfjam extra/cover.pdf extra/blank.pdf \
  extra/task.pdf extra/blank.pdf \
  extra/authorize.pdf \
  extra/zh-abstract.pdf extra/blank.pdf \
  thesis.pdf --outfile final.pdf
