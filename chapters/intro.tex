\chapter{Introduction}
\label{intro}
% Background and motivation.
% At a high level, what is the problem area you are working in and why is it important?
% It is important to set the larger context here.
% Why is the problem of interest and importance to the larger community?

Automation turns craft into industry.
Cars, which used to be crafted by hand, are now made by robots in an assembly line.
The automobile industry has become automated.
The automation of automobile manufacturing increases productivity and quality consistency
while decreases direct human labor costs and expenses;
therefore, the resources which used to be consumed by reptitive tasks become available for 
intellectual creations, such as automobile design.

Computers were invented to relieve people of tedious computations in everyday life and business.
The building of computers began as a craft, but the craft era of computer manufacture
is now coming to an end.  On the other hand, the computer gave birth to a larger craft industry 
that is still flourishing, the software industry \cite[1015]{dyson1998science}.

\cite{mancuso2013software} sees software as a craft and compares software developers to
medieval blacksmiths.  However, software managers, who can be the most amateur of amateurs 
when it comes to programming \cite[124]{weinberg1971psychology}, 
see software development as a production line. 
After decades and many different methodologies, software projects are still failing
\cite[5]{mancuso2013software}.
They rarely fail for just one or two reasons, but insufficient time and budget is a 
frequent and crucial cause in most software projects.

Program synthesis sets as its task the discovering of an executable program based on user intent
expressed in the form of some constraints \cite[13]{gulwani2010dimensions}.
Ideally, it only involves stating \emph{what} is to be computed, but not necessarily 
\emph{how} it is to be computed \cite[94]{lloyd1994practical}.
The synthesized programs are correct-by-construction, without the errors associated
with low-level programming details.
Despite holding the promise of significantly easing the task of programming, 
it has received little attention due to its difficulty \cite[313]{srivastava2010pvps}.

Describing what a computation does is often easier than defining it explicitly. 
That is, we may be able to write down the relation between the \emph{input} and the \emph{output}
easily, even when it is difficult to construct a program to satisfy that relation
\cite[151]{manna1971toward}.
%
In \emph{Programming by Example} (PbE),
a programmer (or even an end user) provides the PbE system with 
examples of a task he or she wishes to perform,
and the PbE system infers a program to accomplish that task.
%
Among various forms of examples, such as logical specifications, 
natural language, traces and programs,
\emph{input-output examples} can act as the simplest form of examples
which are both succinct and precise.

\section{A motivating example}

Suppose we are editing a source code file written in the C programming language.
The whole program is computationally intensive and contains the following code
snippet which is a double loop executed for millions of times.
%
\begin{verbatim}
Ey = Ez = Eyw = Ef = Efw = 1;
for (i = 1; i <= M; i++)
    for (k = 1; k <= N; k++)
    {
        // calculate Sy,Sz,Syw,Sf,Sfw using k
        Ey = Sy + Ey;
        Ez = Sz + Ez;
        Eyw = Syw + Eyw;
        Ef = Sf + Ef;
        Efw = Sfw * Efw;
    }
\end{verbatim}
%
In order to improve the performance of the whole program, we would like to
parallelize the loop in such a way that $Ey,Ez,Eyw,Ef$ and $Efw$ are accumulated
in separate threads and the accumulated results are stored in the memory indexed
by the thread identifiers.  Also we would like to reformat the code to make use of self-assignment
operators in C for brevity.  This transformation process is depicted in \fig{motivation}.

\begin{figure}
\centering
\begin{minipage}{0.3\textwidth}
\begin{verbatim}
Ey = Sy + Ey;
Ez = Sz + Ez;
Eyw = Syw + Eyw;
Ef = Sf + Ef;
Efw = Sfw * Efw;
\end{verbatim}
\end{minipage}
\begin{minipage}{0.1\textwidth}
  $$\Rightarrow$$
\end{minipage}
\begin{minipage}{0.3\textwidth}
\begin{verbatim}
Ey[i] += Sy;
Ez[i] += Sz;
Eyw[i] += Syw;
Ef[i] += Sf;
Efw[i] *= Sfw;
\end{verbatim}
\end{minipage}
\caption{A motivating example}
\label{fig:motivation}
\end{figure}

In order to achieve this transformation, there are three choices described as follows.
\begin{enumerate}
  \item \emph{Manually edit the 5 lines} by moving the cursor to different locations,
    typing in new characters, and deleting unused characters.
    This choice requires several mouse clicks and keystokes, and is not only boring but also time-consuming.
    Also manually editing all the lines does not scale at all when the number of lines to be edited is very large.
  \item \emph{Write a specific transformation program}.  A program easily scales to a large number of input lines.
    As you may find, however, the specific transformation example mentioned above is rare in general programming experience,
    so it is hard to say this program is reusable.  Also it usually takes longer time to debug this program
    than just edit the lines manually if there are only 5 lines.
  \item \emph{Use a shell command} such as sed\footnote{Unix utility; a stream editor for filtering and transforming text.}.
    In order to transform these 5 lines, we can use {\small\verb!sed -r 's/(\w+) = (\w+) (.) \w+/\1[i] \3= \2/'!}\footnote{
    A sed script in the form of {\tt s/regexp/replacement/} attempts to match {\tt regexp} against the pattern space.
    If successful, the portion matched will be replaced by {\tt replacement}.}.
    This seems pretty easy yet demands expertise.
    Novices usually cannot make good use of advanced facilities such as regular expressions.
    Even for experts, it takes time to check the usage of specific options\footnote{
    For example, the sed in Linux accepts {\tt -r} for regular expressions
    while the one in Mac OS X accepts {\tt -E} for a similar functionality.}
    (e.g., \verb!-r! enables regular expressions) and language dialect details\footnote{
    For example, the sed script aformentioned doesn't work in Mac OS X.}
    (e.g., \verb!\1 \2 \3! for back-references).
\end{enumerate}
As we can see, each of these choices has its own difficulties.
Despite the cost of details, using a shell command such as sed helps a lot
because it has domain-specific complexities contained in it.
A sed script is written in a language tailored specifically for string transformations.
However, for such an intuitively simple task in \fig{motivation}, learning
\emph{how} to transform the text using sed is much more expensive than just
specifying \emph{what} we want to get.

\begin{figure}
\centering
\begin{spacing}{1.0}
\begin{tikzpicture}[
    example/.style={rectangle,draw,fill=gray!10,text width=3.5cm},
    system/.style={rectangle,draw,fill=orange!10,align=center,text width=2.5cm},
    program/.style={rectangle,draw,fill=green!10,align=center,text width=2.5cm},
    data/.style={rectangle,draw,align=left,text width=3.5cm}
  ]

  \node[example] (example) {\begin{tabular}{l}
      Example: \\
      \verb!Ey = Sy + Ey;! \\
      $\quad\quad\quad\Downarrow$ \\
      \verb!Ey[i] += Sy;! \\
  \end{tabular}};
  \node[system] (system) [right=of example] {PbE System};
  \node[data] (input) [below=of example] {
  \verb!Ez = Sz + Ez;!
  \verb!Eyw = Syw + Eyw;!
  \verb!Ef = Sf + Ef;!
  \verb!Efw = Sfw * Efw;!};
  \node[program] (program) [right=of input] {Synthesized Program};
  \node[data] (output) [right=of program] {
  \verb!Ez[i] += Sz;!
  \verb!Eyw[i] += Syw;!
  \verb!Ef[i] += Sf;!
  \verb!Efw[i] *= Sfw;!};

  \draw [->] (example) -- (system) -- (program);
  \draw [->] (input) -- (program) -- (output);

\end{tikzpicture}
\end{spacing}
\caption{Text transformation using a programming by example system}
\label{fig:pbe-usage}
\end{figure}

By using the PbE system described in this thesis, we first provide
an example of our intent by editing the first line of the code snippet.
The PbE system takes this example as the input, infers and synthesizes
a transformation program as the output.  The synthesized program takes
the rest of the code snippet as the input, and outputs the expected
transformation result.
This process is depicted in \fig{pbe-usage}. The gray box is the example
provided for the PbE system (the orange box). The green box represents the
synthesized transformation program.  Arrows represent data flow.

For end-user applications, this PbE system can be deployed as a service
of which various user interfaces can make use.  For example, an extension
or a plugin of a screen editor such as vim\footnote{http://www.vim.org/}
and TextMate\footnote{http://macromates.com/ The Missing Editor for Mac OS X}
can invoke the service via Unix sockets.

\section{The problem}
% What is the specific problem considered in this paper?
% This paragraph narrows down the topic area of the paper.
% In the first paragraph you have established general context and importance.
% Here you establish specific context and background.

In this thesis, the problem of synthesizing string transformation programs
using input-ouput examples is studied.
What we are going to build is a PbE system which takes one or more examples
as the input, and outputs a synthesized program which is consistent with
all the provided examples, and hopefully generalizes well to new inputs.
%
However, there are infinite numbers of transformations applicable to a string,
and synthesizing arbitrary string transformations would be intractable.

What we are concerned about in this thesis is the transformations which are \emph{finitely
describable by using a finite set of computable functions}.
This set of functions is fixed and available to the PbE system for a specific
PbE task.  This is a reasonable restriction rather than a limitation because
not only widespread end-user products such as spreadsheet processing systems provide
a fixed set of useful functions, but also general-purpose programming languages
have a limited number of language constructs.
The PbE system does not impose any restrictions on the given functions, but only
requires the knowledge of this specific set of functions to decide the expressibility.
Similar assumptions exist in the literature of learning of languages and grammars,
where the knowledge of the alphabet is known beforehand.

A formal description of the problem discussed in this thesis
is described in Chapter \ref{formulate}.
Next we present previous approaches and our approach to solving this problem.

%\section{Our contributions}
% What are the main contributions of your paper given the context 
% you have established in paragraphs 1 and 2.
% What is the general approach taken? Why are the specific results significant?
\section{The approach}

\subsection{Previous approaches}
Much work has been done in the field of programming by example.
In this section, we introduce three significant approaches to
solving the problem of synthesizing string transformation programs.
Our approach is based on some ideas underlying these existing approaches.
Chapter \ref{related} will discuss these approaches in more details.

\subsubsection{Gap programs} % POPL 1984
Gap programs \citep{nix1984editing} are the class of $pattern\Rightarrow replacement$ programs.
Both the pattern and the replacement are lists of constants and gaps.
A \emph{gap} is like a \emph{group} in extended regular expressions used in sed,
and matches any sequence of characters up to the constant that follows the gap
in the pattern.

Gap patterns are synthesized by first
computing the longest common subsequence (LCS) of the example inputs,
and then performing gap insertion (into the LCS) using a simple heuristic.
For the motivating example depicted in \fig{motivation}, the LCS of the example
inputs is first calculated, which is ``\verb!E = S  E;!".
And then gaps are inserted to get a pattern
``$\texttt{E}g_1\texttt{ = S}g_2\texttt{ }g_3\texttt{ E}g_4\texttt{;}$".

\begin{figure}[t]
\centering
\includegraphics[width=0.8\textwidth]{figures/ebe-automaton}
\caption{Gap replacement automaton for a specific input}
\label{fig:intro-ebe-automaton}
\end{figure}

Gap replacements are synthesized by first
constructing an NFA for every example, which accepts the output using gap contents in the input
(see \fig{intro-ebe-automaton} for an example), and then
intersecting all the automata constructed for all examples as the final replacement automaton.

\subsubsection{String extraction} % POPL 2011

As mentioned in \cite{gulwani2011spreadsheet}, the problem of generating
the output string can be decoupled into independent sub-problems of
generating different parts of the output string.
In a sub-problem, some substrings of the input are extracted and combined
in a new way to generate the output.

A substring is determined by two positions.
Gap programs locate input substrings according to characters in the LCS.
In \cite{gulwani2011spreadsheet}, position locating is improved by
using regular expressions.
A position is located by matching $r_1$ on the left-hand side
and $r_2$ on the right-hand side, which is depicted in \fig{locate-position}
and illustrated in the following example \cite[319]{gulwani2011spreadsheet}.

\begin{figure}[b]
\centering
\begin{tikzpicture}[
    auto,
    empty/.style={inner sep=0pt}
  ]
  \node[empty] (center) {};
  \node[empty] (up) [above=of center] {};
  \node[empty] (down) [below=of center] {the position to locate};
  \node[empty] (left) [left=of center] {};
  \node[empty] (right) [right=of center] {};
  \draw [->] (center) -- node [swap] {$r_1$} (left);
  \draw [->] (center) -- node {$r_2$} (right);
  \draw [dashed] (up) -- (down);
\end{tikzpicture}
\caption{Position locating using two regular expressions}
\label{fig:locate-position}
\end{figure}

\begin{example}
  \label{sumit-example}
  The goal in this problem is to extract the quantity of the purchase.
  \begin{center}
    \begin{tabular}{ll}
      \hline
      Input & Output \\
      \hline
      BTR KRNL WK CORN 15Z & 15Z \\
   CAMP DRY DBL NDL 3.6 OZ & 3.6 OZ \\
 CHORE BOY HD SC SPNG 1 PK & 1 PK \\
 FRENCH WORCESTERSHIRE 5 Z & 5 Z \\
     O F TOMATO PASTE 6 OZ & 6 OZ \\
      \hline
    \end{tabular}
  \end{center}
  The following string program identifies the left position to be the
  one before the occurrence of the first number, while the right position
  to be the one at the end of the string. \\
  \underline{String Program:}
  $\texttt{SubStr}\,(\texttt{Pos}\,(\epsilon,[0-9]),\texttt{ConstPos}\,(-1))$
\end{example}

\subsubsection{Guided search of function compositions} % ICML 2013

\cite{menon2013icml} do not extract substrings as is described in the two approaches mentioned above, but purely rely on
the composition of (potentially arbitrary) functions.
The following example \cite[2]{menon2013icml} illustrates the way that function compositions are used.
%
\begin{example}
  \label{intro-icml-example}
  Imagine a user has a long list of names with some repeated entries
  (say, the Oscar winners for Best Actor), and would like to create
  a list of the unique names, each annotated with their number of occurrences.
  \begin{center}\tt\small
  \begin{tabular}{ccc}
    \begin{tabular}{|l|}
      \hline
      Anthony Hopkins \\
      Al Pacino \\
      Tom Hanks \\
      Tom Hanks \\
      Nicolas Cage \\
      \hline
    \end{tabular}
    & $\to$ &
    \begin{tabular}{|l|}
      \hline
      Anthony Hopkins (1) \\
      Al Pacino (1) \\
      Tom Hanks (2) \\
      Nicolas Cage (1) \\
      \hline
    \end{tabular}
  \end{tabular}
  \end{center}
  The corresponding program $f(\cdot)$ may be expressed as the composition
  \begin{equation}
    f(x) = dedup(concatLists(x, ``~", concatLists(``(",count(x,x),``)"))).
  \end{equation}
  The argument $x$ represents the list of input lines that the user wishes to process.
\end{example}
%
Brute-force search of consistent compositions is intractable in general.  However,
certain \emph{textual features} can help bias the search process by providing
\emph{clues} about which functions \emph{may} (but are not guaranteed to) be relevant.
For instance, in Example \ref{intro-icml-example}, (a) there are duplicate lines in the
input but not the output, suggesting that the function $dedup$ may be useful,
(b) there are many more spaces in the output than in the input, suggesting that
the constant string ``~" (space) may be useful.
By \emph{learning} weights that tell us the reliability of these clues,
the inference process can be speeded up significantly over brute-force search
\cite[3]{menon2013icml}.

\subsection{Our approach}

Our approach is based on some ideas underlying these previous approaches.
The advantages and disadvantages of these three approaches are described as follows.
\begin{description}\itemsep-1mm
  \item[Gap programs] separate parsing and output generation,
    but they have very limited expressibility, and fail in most real-world cases.
  \item[Substring extraction] decides positions by using regular expressions,
    but the learning algorithm is very slow and impractical for large-size programs.
  \item[Guided search of function compositions] allows arbitrary transformation functions,
    but it is unable to handle infinite streams and requires extra knowledge.
\end{description}
%
Our approach combines the advantages and avoids the disadvantages
by using symbolic transducers\cite[1]{transducer2012} extended with registers (infinite buffers), whose transitions include
position conditions such as $(r_1,r_2)$ where $r_1$ and $r_2$ are regular expressions.
%
We synthesize transducers with two phases as depicted in \fig{intro-struct-st}.
\begin{spacing}{1.0}
\begin{enumerate}
  \item In the \emph{parsing phase}, input symbols are appended to different registers.
  \item In the \emph{output phase}, registers are transformed and combined in some order.
\end{enumerate}
\end{spacing}
%
\begin{figure}[h]
\centering
\input{figures/synthesized-transducer}
\caption{The structure of synthesized transducers}
\label{fig:intro-struct-st}
\end{figure}

\begin{figure}
\centering
\input{figures/frontend}
\caption{Data structures in the process of parsing phase synthesis}
\label{fig:ppsyn-process}
\end{figure}

\fig{ppsyn-process} shows the process of synthesizing the parsing phase, a.k.a. the \emph{frontend}.
Given a set of input-output examples, we first find the correspondences between
the input and the output for each example.
Having these substring correspondences, we can build conjectures on potentially
useful positions.
By using the most similar subsequence algorithm described later in this thesis,
we combine all the position conjectures into one position conjecture.
In order to make this combined conjecture to be minimal and consistent, we then use
the training example inputs (and several test inputs, if provided) to disambiguate the conjecture.
Finally, we transform the conjecture into a parsing automaton, a.k.a. the parser.

\begin{figure}
\centering
\input{figures/backend}
\caption{Data structures in the process of output phase synthesis}
\label{fig:outsyn-process}
\end{figure}

\fig{outsyn-process} shows the process of synthesizing the output phase, a.k.a. the \emph{backend}.
Given the set of examples and the parser, we first parse all the example inputs to get the parse results.
Then we use the results and the example outputs to generate output conjectures.
An output conjecture is an automaton like the one shown in \fig{intro-ebe-automaton}.
After that, all the conjectures are intersected to get one conjecture.
This conjecture is disambiguated using some heuristics and transformed to an output transducer.

By using symbolic transducers extended with registers,
we have powerful expressivity since such transducers are Turing-complete, and also
have many opportunities in further optimization.
A detailed discussion of our approach is presented in Chapter \ref{approach}.

\section{The thesis}
Symbolic transducers extended with registers are feasible and useful for
synthesizing string transformation programs using input-output examples.

%\section{Prior work on program synthesis}
% At a high level what are the differences in what you are doing, and what others have done?
% Keep this at a high level, you can refer to a future section where specific details 
% and differences will be given. But it is important for the reader to know at a high level, 
% what is new about this work compared to other work in the area.

\section{Overview of the rest of this thesis}
% Give the reader a roadmap for the rest of the paper.
% Avoid redundant phrasing, "In Section 2, In section 3, ... In Section 4, ... " etc.
In Chapter \ref{related}, we present some related work in details.
Chapter \ref{formulate} formulates the problem to solve, and
Chapter \ref{approach} presents our approach.
The implementation and evaluation of our system is described in
Chapter \ref{implement} and Chapter \ref{eval}.
Finally, Chapter \ref{conclusion} concludes this thesis.
