\chapter{Related Work}
\label{related}

Much work has been done in the field of programming by demonstration \citep{wwid1993}
and programming by example \citep{yourwish2001}.  However, most of them focus on program transcription 
instead of example generalization.

Examples reflect the user's intent, and a good generalization
of the examples is a good understanding of that intent.
For each input-output example provided by
the user, the synthesizer needs to figure out \emph{how} the input is transformed to the output.
In general, the input has structures in it, and the output resembles the input structures in another way.
Transformations in such a flavor typically involve two decoupled phases:
\begin{enumerate}\itemsep0em
  \item Parse the input into pieces and structures
  \item Transform the structures separately, and concatenate the transformed pieces in a specific manner
\end{enumerate}
In order to parse future user inputs correctly, we need a parsing procedure which is consistent for at least
all the given inputs.

String transformation and text processing problems can also be handled through programming
by using languages and tools such as C, awk, shell commands, etc.  Each of these has its own difficulties
and demands expertise, while in this thesis, the user is supposed to know nothing but
\emph{what} he or she wants to accomplish.
For that reason, we do not consider the languages and tools aforementioned as related.

In this chapter, we present several existing program synthesis systems related to string transformation and text processing,
and an brief discussion of grammatical inference.

\section{Program synthesis systems}

There are two major categories of program synthesis systems for end users to specifc \emph{what} they want to do.
One is \emph{Programming by Examples}, where a user only provides examples of the inputs and the expected outputs.
The other one is \emph{Programming by Demonstration}, where a user also provides intermediate states in the process
of transforming the inputs to the outputs.  Such intermediate states are called \emph{traces}.

For example, in string transformation program synthesis, PbE users provide the system with strings before and
after the transformation, while PbD users provide more information such as cursor moves, keystrokes, etc.
Table \ref{tab:pbd-traces} shows a possible trace that the user may provide to the system.

\begin{table}
\centering
\caption{A possible trace provided to PbD system}
\label{tab:pbd-traces}
\begin{tabular}{ll|ll}
  \hline
    Keystroke & State &
    Keystroke & State \\
  \hline
      \RArrow & \verb!E|y = Sy + Ey;! &
      \RArrow & \verb!Ey[i] += |Sy + Ey;! \\
      \RArrow & \verb!Ey| = Sy + Ey;! &
      \RArrow & \verb!Ey[i] += S|y + Ey;! \\
\keystroke{[} & \verb!Ey[| = Sy + Ey;! &
      \RArrow & \verb!Ey[i] += Sy| + Ey;! \\
\keystroke{i} & \verb!Ey[i| = Sy + Ey;! &
         \Del & \verb!Ey[i] += Sy|+ Ey;! \\
\keystroke{]} & \verb!Ey[i]| = Sy + Ey;! &
         \Del & \verb!Ey[i] += Sy| Ey;! \\
      \RArrow & \verb!Ey[i] |= Sy + Ey;! &
         \Del & \verb!Ey[i] += Sy|Ey;! \\
\keystroke{+} & \verb!Ey[i] +|= Sy + Ey;! &
         \Del & \verb!Ey[i] += Sy|y;! \\
      \RArrow & \verb!Ey[i] +=| Sy + Ey;! &
         \Del & \verb!Ey[i] += Sy|;! \\
  \hline
\end{tabular}
\end{table}

Traces provide more information of user intent than just input-outputs, but it's not easy to make full use of
the extra information.
Also, for a given pair of input and output, there are infinite number of consistent traces, most of which
are noises.
For example, the user may move the cursor back and forth without typing any characters.
Such repetitions can generate an infinite series of consistent traces.
This is not a made-up scenario because it is usual to make mistakes such as moving the cursor a bit
further than the expected position, and the actions for moving the cursor backwards are included in the final trace.
In summary, PbD based systems have the drawback of being sensitive to the order
in which the user chooses to perform actions \cite[328]{gulwani2011spreadsheet}.

Next we first present three PbE approaches for synthesizing string transformations,
and then give a brief introduction to several PbD systems.

\subsection{Program synthesis from I/O behavior}

\subsubsection{Gap programs} % POPL 1984
Gap programs \citep{nix1984editing} are the class of $pattern\Rightarrow replacement$ programs.
Gap patterns define languages that are a proper subclass of the regular
languages, and parse strings uniquely into constants and gaps.
A \emph{gap} is like a \emph{group} in extended regular expressions used in sed,
and matches any sequence of characters up to the constant that follows the gap
in the pattern.

Just like a gap pattern, a replacement is also a list of constants and gaps.
In a gap pattern, gaps are distinct symbols drawn from a gap alphabet,
and are not allowed to appear more than once, while in a gap replacement,
constants and gaps can appear arbitrary times in arbitrary order.
Another limitation of gap patterns is that gap symbols must be separated by
a constant string, which means that ``\verb!this -1- -2- not a pattern!"
is an invalid gap pattern, where ``\verb!-1-!" and ``\verb!-2-!" are gaps.

Gap patterns are synthesized by following two steps:
\begin{enumerate}\itemsep0em
  \item Compute the longest common subsequence (LCS) of the example inputs
  \item Perform gap insertion (into the LCS) using a simple heuristic
\end{enumerate}
For the motivating example depicted in \fig{motivation}, the LCS of the example
inputs is first calculated, which is ``\verb!E = S  E;!".
Then all the example inputs are parsed and gaps are inserted as is shown in Table \ref{tab:ebe-parses}.

\begin{table}[b]
\centering
\caption{Input parse results using gap patterns}
\label{tab:ebe-parses}
\begin{tabular}{ccccccccc}
  \hline
  0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 \\
  \hline
  $\epsilon$ & y & $\epsilon$ & $\epsilon$ & $\epsilon$ & y & + & $\epsilon$ & y \\
  $\epsilon$ & z & $\epsilon$ & $\epsilon$ & $\epsilon$ & z & + & $\epsilon$ & z \\
  $\epsilon$ & yw & $\epsilon$ & $\epsilon$ & $\epsilon$ & yw & + & $\epsilon$ & yw \\
  $\epsilon$ & f & $\epsilon$ & $\epsilon$ & $\epsilon$ & f & + & $\epsilon$ & f \\
  $\epsilon$ & fw & $\epsilon$ & $\epsilon$ & $\epsilon$ & fw & * & $\epsilon$ & fw \\
  \hline
\end{tabular}
\end{table}

Gap replacements are synthesized by following two steps:
\begin{enumerate}\itemsep0em
  \item Construct an NFA for every example, which accepts the output using gap contents in the input
  \item Intersect all the automata constructed for all examples as the final replacement automaton
\end{enumerate}
An NFA example is shown in \fig{ebe-automaton}, which is for
the input ``\verb!Ey = Sy + Ey;!" and the output ``\verb!Ey[i] += Sy;!".
\begin{figure}[t]
\centering
\includegraphics[width=0.8\textwidth]{figures/ebe-automaton}
\caption{Gap replacement automaton for a specific input}
\label{fig:ebe-automaton}
\end{figure}

\subsubsection{String extraction} % POPL 2011

As mentioned in \cite{gulwani2011spreadsheet}, the problem of generating
the output string can be decoupled into independent sub-problems of
generating different parts of the output string.
In a sub-problem, some substrings of the input are extracted and combined
in a new way to generate the output.
A substring is determined by two positions and position locating is improved by
using regular expressions.

\begin{figure}[b]
\centering
\begin{tabular}{|crclc|}
  \hline
  & & & & \\
  & String expr $e$ & $:=$ & $\texttt{\small Concatenate}\,(f_1,\cdots,f_n)$ & \\
  & Atomic expr $f$ & $:=$ & $\texttt{\small ConstStr}\,(s)$ & \\
  &                 & $|$  & $\texttt{\small SubStr}\,(p_1,p_2)$ & \\
  &    Position $p$ & $:=$ & $\texttt{\small ConstPos}\,(k)$ & \\
  &                 & $|$  & $\texttt{\small Pos}\,(r_1,r_2)$ & \\
  & & & & \\
  \hline
\end{tabular}
\caption{Syntax of major language constructs of string expressions}
\label{fig:sumit-syntax}
\end{figure}

\fig{sumit-syntax} shows the syntax of major language constructs of
string expressions proposed in \cite{gulwani2011spreadsheet}.
$s$ is a constant string and $k$ is a constant integer.
$r_1$ and $r_2$ are restricted regular expressions.
A position is located by matching $r_1$ on the left-hand side
and $r_2$ on the right-hand side, which is depicted in \fig{locate-position}.

As is shown in Example \ref{sumit-example}, the synthesized program is a composition of functions,
as opposed to separated phases of matching and replacing in gap programs.
Both of them only allow direct copy from the input to the output without further transformations
on extracted substrings.

\cite{gulwani2011spreadsheet} has a major disadvantage that
the presented learning algorithm is incomplete, and the synthesized
position locating program is not general enough.
For example, in the proposed string expression language, a non-constant position
is actually a function $\texttt{Pos}\,(r_1,r_2,c)$ where $c$ is the number of times
matching $r_1$ and $r_2$.  We omit $c$ in previous examples for brevity.
However, this $c$ value is always a constant in the proposed learning algorithm,
meaning that positions are decided independently.  If we want to extract from
$aaabbbaaa$ the second $aaa$, independent position locating must
conform to the 4th occurrence of $a$, instead of the first occurrence of $a$
after matching $bbb$.  For a new input such as $aaaabbbaaa$ or $cccbbbaaa$,
the 4th occurrence of $a$ is obviously the wrong position.

\subsubsection{Guided search of function compositions} % ICML 2013

\cite{menon2013icml} do not extract substrings as is described in the two approaches mentioned above, but purely rely on
the composition of (potentially arbitrary) functions.  The types in the proposed language
also are not limited to positions and strings, but allow arbitary types such as lists of strings.
In Chapter \ref{intro}, Example \ref{intro-icml-example} already illustrates the way that
function compositions are used.

This work also includes a set of 280 examples with both an example pair $(\bar{x},\bar{y})$
and evaluation pair $(x,y)$ specified.  We manually checked all these examples one by one,
fixed several corrupted examples, and removed unrelated ones.  This test suite will be used
in the evaluation of the system described in this thesis.

\subsection{Program synthesis from traces}

Mo's TELS system \cite[1]{TELS} generalizes users' iterative operations by using a set of
heuristic/expert rules and infers an editing procedure including loops and conditional branches.
However, TELS's dependence on heuristic rules to describe the possible generalizations makes
it difficult to understand the hypothesis space clearly, as well as to imagine applying
it to the different domain of spreadsheet applications \cite[328]{gulwani2011spreadsheet}.

SMARTedit \cite[1]{lau2001programming} is a PbD system for learning text-editing commands,
where the primitive program statements include moving the cursor to a new position and
inserting/deleting characters.
However, the language of programs considered is not expressive enough.
For example, it does not support conditionals.
Also, its cursor movement logic is restricted to positions either
before or after the $k$-th occurrences of a single token, while scanning from left side.

Simultaneous editing \cite[1]{simuledit} ia another PbD-like system that allows the user
to define a set of regions to edit, and then allows the user to make edits in one, while
the system makes similar editing in all other regions.  The inference used is much less
powerful since every editing action is applied uniformly to every region,
and it does not support conditonal or loopy edits.

\section{Grammatical inference}
Classic grammar induction algorithms are divided into two categories.
One category is those that require both positive and negative examples
to infer a grammar.  The other category is those that only require
positive examples.  Negative examples are not suitable to be provided
by end-users in practice.
Unfortunately, an early result by \cite{gold1967language} showed that
minimal grammar induction is impossible for any superfinite class of
languages providing no access to negative examples \cite[447]{gold1967language}.
A superfinite class of languages is any set of languages that includes
all finite languages and at least one infinite language. Hence,
regular expressions and context free grammars are superfinite.

There are in general two ways to avoid this negative result: (1) use
domain-specific knowledge to limit the class of languages to a
non-superfinite class, or (2) give up on minimal language
identification and use approximations.
Given the difficulty of finding useful non-superfinite language
classes, it is reasonable to use approximations.
For example \cite{fisher2008pads} propose a method for infering
data formats from ad hoc data sources.  However, a larger number of
examples is available from these data sources than our scenario.

Despite the negative results from \cite{gold1967language},
grammatical inference still develops in several other aspects.
Identification in the limit is only of the learning models.
Other models include teachers and queries, and PAC learning.

Angluin describes six types of queries that can be asked of
the teachers (i.e. oracles), two of which have a significant impact on
language learning: membership and equivalence \cite[1]{angluin1988queries}.
A teacher that answers both membership and equivalence queries is
sufficient to help identify DFAs in polynomial time \cite[1]{angluin1987learning}.
Other work of Angluin 
\cite[1]{angluin1980inductive}
\cite[1]{angluin1982inference}
\cite[1]{angluin1990negative}
also provide important results.

Valiant proposed the Probably Approximately Correct (PAC) learning model
\cite[1]{valiant1984theory} in 1984.
This model has elements of both identification in the limit and learning from an oracle,
but differs because it doesn’t guarantee exact identification with certainty.
The novelty and uniqueness of Valiant's model intrigued the grammatical
inference community, but negative and NP-hardness equivalence results
\cite[1]{kearns1987learnability} \cite[1]{pitt1988computational} dampened enthusiasm for PAC learning.

Li and Vitanyi propose a modification to the PAC learning model that only
considers simple distributions \cite[1]{li1991learning}.
These distributions given by the authors
are polynomially learnable but not known to be
polynomially learnable under Valiant's more general distribution assumptions.

There are many surveys
\cite[1]{lee1996learning}
\cite[1]{sakakibara1997recent}
\cite[1]{de2000current}
\cite[1]{de2005bibliographical}
\cite[1]{stevenson2013grammatical}
introduce the theory of grammatical inference and review
the state of the arts in more details.
