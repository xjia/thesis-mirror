\chapter{Problem Formulation}
\label{formulate}

In this chapter, we formulate the problems of programming by example
and string transformation program synthesis by example, and show that
both of these two problems are intractable.

\begin{problem}[Programming by Example]\label{prob:pbe}
  Assume a user intent represented as a function $f:X\to Y$
  where $X$ and $Y$ are the domain and the range of $f$, respectively.
  Given a finite set of input-output examples $S\subset X\times Y$ such that
  $\forall (x,y)\in S, y=f(x),$
  find a function $\hat{f}$ such that 
  $\forall x\in X, \hat{f}(x)=f(x).$
\end{problem}

By this definition, it is obvious that a Programming by Example 
(as opposed to Demonstration) system 
only requires the user to provide the final state
(as opposed to also providing the intermediate states).

\begin{theorem}\label{thm:pbe}
The Programming by Example problem is intractable.
\begin{proof}
We prove by reducing the problem of listing all permutations (all possible orderings)
of $n$ numbers to the programming by example problem.
The former problem of listing all permutations requires non-polynomial amount of output,
so it clearly will take a non-polynomial amount of time.

The problem of listing all permutations of $n$ numbers is a function $f$
such that $f(n)$ is the set of all permutations of $\{1,2,\cdots,n\}$.
A permutation is represented as a list, e.g. $[1,2,\cdots,n]$.
So
\begin{equation}
  f(3)=\{[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]\}.
\end{equation}
Now we provide a set of examples $S=\{(3,f(3))\}$.
If the function $\hat{f}$ can be found in polynomial time,
then $f$ is computable in polynomial time, which is a contradiction.
\end{proof}
\end{theorem}

The Programming by Example problem is specialized to the string transformation
program synthesis problem as follows.

\begin{problem}[String Transformation Program Synthesis]
  Assume a string transformation program represented as a function $f:\Sigma^*\to\Gamma^*$
  where $\Sigma$ and $\Gamma$ are finite sets of symbols, i.e. the alphabets.
  Given a finite set of input-output examples $S\subset\Sigma^*\times\Gamma^*$ such that
  $\forall (x,y)\in S, y=f(x),$
  find a function $\hat{f}$ such that
  $\forall x\in\Sigma^*, \hat{f}(x)=f(x).$
\end{problem}

The actual meaning of $\Sigma$ is arbitrary, e.g. integers, characters, tokens, etc.

\begin{theorem}
The string transformation program synthesis problem is intractable.
\begin{proof}
We prove by following the proof of Theorem \ref{thm:pbe}.
Let $\Sigma=\{0,1,2,\cdots,9\}$ and $\Gamma=\Sigma\cup\{[,],:\}$.
Hence, an integer $n$ can be represented by a string under the alphabet $\Sigma$.
And we have
\begin{equation}
  f(``3")=``[1:2:3]:[1:3:2]:[2:1:3]:[2:3:1]:[3:1:2]:[3:2:1]".
\end{equation}
So it is obvious that a set of permutations can also be represented as
a string under the alphabet $\Gamma$.
This encoding process can be done in polynomial time, and it is also
reversible in polynomial time.
So this problem is equivalent to Problem \ref{prob:pbe}.
\end{proof}
\end{theorem}

