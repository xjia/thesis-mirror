\chapter{Implementation}
\label{implement}

We implement our system in Standard ML\footnote{http://www.standardml.org/}.
In this chapter, we first present the general code structure,
important types, and representations of parsers and automata.
Then one of the most important algorithms in our approach,
the most similar subsequence algorithm, is implemented in the text.
%Finally, some functional pearls are showcased.

\section{General code structure}

In order to make our system general enough and to make the implementation
reusable, the sepcification of the synthesis problem is separated from the
concrete implementation.  We also define a meta-specification of the desired
user-provided specification, which is implemented as a signature in Standard ML.

\begin{figure}
\lstinputlisting[language=ML,label=lst:signature,caption={Meta-specification of synthesis problems}]{figures/signature.sml}
\end{figure}

Listing \ref{lst:signature} shows the meta-specification of synthesis problems.
A specification of a synthesis problem must provide the system with the input
alphabet, the output alphabet, the set of useful functions, and a character
generalization hierarchy.  Useful functions are partitioned into different
groups according to their arities.
We also define two auxiliary types, \verb!input_substr! and
\verb!output_substr!, for the readability of substring-related functions.

\begin{figure}
\lstinputlisting[language=ML,label=lst:functor,caption={The synthesizer functor}]{figures/functor.sml}
\end{figure}

The implementation of our approach is programmed against the signature named
\verb!SYNTHESIS_PROBLEM!, by using the functor construct provided in Standard
ML, which is shown in Listing \ref{lst:functor}.
A functor acts like a parameterized module, which turns structures into
structures.  That is, a functor accepts one or more parameters, which are
structures of a given signature, and produces a structure as its result.
Functors are used to implement generic algorithms and data structures.

Our system is implemented in the functor named \verb!SynthesizerFn!, which takes
a specification of a synthesis problem as the input, and returns a synthesizer.
The synthesizer contains a function named \verb!synthesizedProgram!, which takes
a list of examples as the input, and returns a synthesized transducer.
Several other important functions are also listed in Listing \ref{lst:functor},
though the details of their implementation are omitted for brevity.

\begin{figure}
\lstinputlisting[language=ML,label=lst:trivial,caption={A trivial synthesizer}]{figures/trivial.sml}
\end{figure}

Listing \ref{lst:trivial} shows an example of using the synthesis problem
meta-specification and our synthesizer functor.  This example illustrates a
trivial synthesis problem, where both the input and the output alphabets are
ASCII characters, and the only useful function is the identity function.
The provided character generalization hierarchy is the same as the one shown
in Example \ref{ex-simple-cgh}.

\section{Types and internal representations}

\begin{figure}
\lstinputlisting[language=ML,label=lst:types,caption={Types of correspondences and position conjectures, and the internal representation of synthesized parsers}]{figures/types.sml}
\end{figure}

Listing \ref{lst:types} shows the types of correspondences and position
conjectures, and the internal representation of synthesized parsers.  Formally,
a correspondence is a triple of the input substrings, the output substirng, and
the transformation function.  However, in the implementation, the transformation
function is not recorded as it is not needed in the frontend.  Also note that
the types of the input substrings and the output substring are
\verb!P.input_substr! and \verb!P.output_substr!, respectively, instead of
the verbose types of \verb!P.X list! and \verb!P.Y list!.  This kind of typing
information can improve the readability of this implementation.

Synthesized parsers are represented as \emph{closures}, as opposed to automata
described in Chapter \ref{approach}.  We choose this representation due to its
simplicity.  Also, automata must be transformed to an executable form in the
context of a running program on a real machine, as opposed to a imaginery one.

An automaton is represented as a list of states, where each state
is a list of transitions.  A transition is a pair of the label and
the number of states to drop to get to the next state.
\begin{example}
  The following automaton
  \begin{center}
    \begin{tikzpicture}[auto]
      \node[circle,draw] (x) {$x$};
      \node[circle,draw] (y) [right=of x] {$y$};
      \node[circle,draw] (z) [right=of y] {$z$};
      \draw [->] (x) -- node{$a$} (y);
      \draw [->] (y) -- node{$b$} (z);
      \draw [->] (x) to[bend right] node[swap]{$c$} (z);
    \end{tikzpicture}
  \end{center}
  is represented as
\begin{verbatim}
[ [(a,1), (c,2)],
  [(b,1)],
  []
]
\end{verbatim}
  To explain this, note that when the head of the list is $x$,
  we need to drop 1 state to get to $y$, and 2 states to get to $z$.
\end{example}

\section{Most similar subsequence}

\begin{figure}
\lstinputlisting[language=ML,label=lst:mss,caption={The implementation of the most similar subsequence algorithm}]{figures/mss.sml}
\end{figure}

Listing \ref{lst:mss} shows the implementation of the most similar subsequence algorithm.
The whole algorithm is divided into two phases.  In the first phase, the lengths in MSS subproblems
are calculated, which is described in Algorithm \ref{alg:mss-length}.
In the second phase, an MSS is constructed according to Algorithm \ref{alg:print-mss}.

The $b$ and $c$ tables are stored separately in Algorithm \ref{alg:mss-length},
while in the implementation, the $b$ and $c$ values of the same cell coordinate
are stored together using a pair.  The \verb!datatype! declaration of \verb!direction! in Listing \ref{lst:mss}
is the type of $b$ values, where \verb!left! corresponds to $``\leftarrow"$ and
\verb!diag! corresponds to $``\nwarrow"$.

%\section{Functional pearls}
%
%\begin{figure}
%\lstinputlisting[language=ML,label=lst:sort,caption={A higher-order function for sorting a list with respect to a function}]{figures/sort.sml}
%\end{figure}
%
%\begin{figure}
%\lstinputlisting[language=ML,label=lst:lcs,caption={An implementation of the longest common subsequence algorithm without intermediate tables}]{figures/lcs.sml}
%\end{figure}

