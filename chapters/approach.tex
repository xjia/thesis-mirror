\chapter{Approach}
\label{approach}

\begin{figure}
\centering
\input{figures/frontend}
\caption{Frontend data structures}
\label{fig:frontend-ds}
\end{figure}

\begin{figure}
\centering
\input{figures/backend}
\caption{Backend data structures}
\label{fig:backend-ds}
\end{figure}

\begin{figure}
\centering
\input{figures/synthesized-transducer}
\caption{Synthesized transducer}
\label{fig:synthesized-transducer}
\end{figure}

In this chapter, we describe the details of our approach.
From a high-level point of view, we first synthesize the parser for the user inputs,
and then the output transducer.  Finally, these two components are concatenated together
to get the synthesized transducer.
\fig{frontend-ds} shows the data structures used in the process of synthesizing the parser.
\fig{backend-ds} shows the data structures used in the process of synthesizing the output
transducer.  \fig{synthesized-transducer} shows the structure of synthesized transducers.
Here we call the parser the \emph{frontend} and the output transducer the \emph{backend}.

Given a set of input-output examples, we first find the correspondences between
the input and the output for each example.
Having these substring correspondences, we can build conjectures on potentially
useful positions.
By using the most similar subsequence algorithm described later in this thesis,
we combine all the position conjectures into one position conjecture.
In order to make this combined conjecture to be minimal and consistent, we then use
the training example inputs (and several test inputs, if provided) to disambiguate the conjecture.
Finally, we transform the conjecture into a parsing automaton, a.k.a. the parser.

Given the set of examples and the parser, we first parse all the example inputs to get the parse results.
Then we use the results and the example outputs to generate output conjectures.
An output conjecture is an automaton like the one shown in \fig{ebe-automaton}.
After that, all the conjectures are intersected to get one conjecture.
This conjecture is disambiguated using some heuristics and transformed to an output transducer.

\section{Finding input-output correspondences}

The first step in our approach is to find the correspondences between input substrings
and output substrings.  Intuitively, a correspondence includes a transformation function,
related input substrings and their positions, and the corresponding output substring and
its position.  Note that in our approach, a transformation function may take one or more
parameters, and the internal computation of the transformation function is not restricted
to any form.

The most trivial transformation function is the identity function $\lambda x.x,$
which returns the input directly without any modification.  When the only useful function
is the identity function, our approach falls back to a system which only allows direct
copy from the input to the output, though the positions of the substrings may change.

\begin{example}
\label{ex-swap}
In this example, we illustrate how substrings can be swapped to get an output.
Suppose the user provides an example:
\begin{center}
\begin{minipage}{0.9\textwidth}
\begin{verbatim}
0                   1        0                   1
0 1 2 3 4 5 6 7 8 9 0 1      0 1 2 3 4 5 6 7 8 9 0 1
 h e l l o _ w o r l d   =>   w o r l d _ h e l l o
\end{verbatim}
\end{minipage}
\end{center}
Let $x,y$ denote the input and the output, respectively.
$z[i,j]$ means the substring from position $i$ to $j$ in a string $z$.
For example, $x[3,10]$ is \verb!lo_worl!.

We find correspondences using only the unary identity function,
and start from the position just before the character \verb!w! in the output.
In order to get meaningful correspondences, we employ a heuristic that
longest matching substrings are selected, which means we try to consider an output substring
as long as possible, thus the following list of considered output substrings.
\begin{verbatim}
world_hello
world_hell
world_hel
world_he
world_h
world_
world
\end{verbatim}
As soon as we consider \verb!world! as the output substring, we find a correspondence
between $x[6,11]$ and $y[0,5]$.
Next we start from position $5$ in the output string $y$.
\begin{verbatim}
_hello
_hell
_hel
_he
_h
_
\end{verbatim}
Now we find another correspondence between $x[5,6]$ and $y[5,6]$.
Then we start from position $6$ in the output again, and check whether \verb!hello!
(the longest substring starting from position $6$) has a correspondence in $x$.
Luckily, there is a correspondence between $x[0,5]$ and $y[6,11]$.
Finally, we get a table of correspondences.
\begin{center}\rm
  \begin{tabular}{lll}
    \hline
    Input substrings & Output substring & Transformation function \\
    \hline
    $x[6,11]$ & $y[0,5]$ & identity function \\
     $x[5,6]$ & $y[5,6]$ & identity function \\
     $x[0,5]$ & $y[6,11]$ & identity function \\
    \hline
  \end{tabular}
\end{center}
\end{example}

As illustrated in Example \ref{ex-swap}, substring swapping and concatenation
are \emph{not} transformations.  The only transformation function used in this
example is the identity function, which only copies the content directly from
the input to the output.  Next we introduce two examples which are more complicated
than the identity function.

\begin{example}
In this example, the user wishes to transform a list of numerical dates to a
more human-readable form described as follows.
\begin{center}
\begin{minipage}{0.95\textwidth}
\begin{verbatim}
0                   1      0                   1
0 1 2 3 4 5 6 7 8 9 0      0 1 2 3 4 5 6 7 8 9 0 1 2 3
 2 0 1 0 - 0 5 - 2 1   =>   M a y   2 1 s t   2 0 1 0
\end{verbatim}
\end{minipage}
\end{center}
In addition to the identity function, we introduce two more functions,
(i) \verb!monthify! turns an integer between 1 and 12 to a name of a month,
e.g. \verb!monthify(5)="May"!,
(ii) \verb!ordinalize! turns an integer to its ordinal form,
e.g. \verb!ordinalize(1)="1st"!, \verb!ordinalize(2)="2nd"!, etc.
We still try to find a correspondence with longest substring in the output.
\begin{center}\rm
  \begin{tabular}{lll}
    \hline
    Input substrings & Output substring & Transformation function \\
    \hline
    $x[5,7]$ & $y[0,3]$ & \verb!monthify! \\
   $x[8,10]$ & $y[4,8]$ & \verb!ordinalize! \\
    $x[0,4]$ & $y[9,13]$ & identity function \\
    \hline
  \end{tabular}
\end{center}
The characters in the output which are not included in any correspondences
are treated as constants.  In this specific example, the spaces in the output
are treated as constants.
\end{example}

\begin{example}
In this example, we introduce a new function $add(x,y)=x+y$ for adding two
integers together.  The user provides an example as follows.
\begin{center}
\begin{minipage}{0.85\textwidth}
\begin{verbatim}
0                        0                   1
0 1 2 3 4 5 6 7 8 9      0 1 2 3 4 5 6 7 8 9 0 1
 1 2 3 + 7 8 9 = ?   =>   1 2 3 + 7 8 9 = 9 1 2
\end{verbatim}
\end{minipage}
\end{center}
The correspondences are listed below.
\begin{center}\rm
  \begin{tabular}{lll}
    \hline
    Input substrings & Output substring & Transformation function \\
    \hline
    $x[0,8]$ & $y[0,8]$ & indentity function \\
    $x[0,3],x[4,7]$ & $y[8,11]$ & $add$ \\
    \hline
  \end{tabular}
\end{center}
Note that input substrings may be used multiple times.
\end{example}

Formally, a correspondence is a triple $\langle \vec{x},\hat{y},f\rangle$ where
$\vec{x}$ is the list of input substrings, $\hat{y}$ is the output substring, and
$f$ is the transformation function such that $f(\vec{x})=\hat{y}.$
A substring contains the content of that string along with its starting position.
In order to find all the correspondences between the input and the output,
we try different functions against different combinations of the input substrings.

Functions are examined in batches, and functions with the same arity are examined
in the same batch.  For a fixed arity $k$, we define a $k$-correspondence as a
correspondence such that $|\vec{x}|=k$.  To find all $k$-correspondences between
the input and the output, we first enumerate all the combinations of exactly $k$
substrings in the input.  For any such combination, the substrings should be
disjoint in the input.  We call such input substrings as \emph{disjoint segments}.

In order to find all combinations of $k$ disjoint segments, we define two mutually
recursive functions $f$ and $g$.  The notation of these two functions are used
locally here for brevity, and should not be confused with other occurrences of
$f$ and $g$ elsewhere in this thesis.
A substring is represented as a pair $\langle q,p\rangle$ where $q$ is the content of the string,
and $p$ is the starting position of $q$ in the context of the larger string.
A combination is represented as a set of such pairs.

Let $f(k,s,p)$ denote all the combinations of drawing $k$ disjoint segments from $s$,
where $s$ is the substring starting from $p$ in the larger string.
Let $g(k,s,p)$ denote all the combinations of drawing $k$ disjoint segments from $s$
with the restriction that the first segment starts exactly at the beginning of $s$,
i.e. at position $p$.
Then the two functions $f$ and $g$, and related functions, can be defined as follows.
%
\begin{equation}
  \label{eqn:f-ksp}
  f(k,s,p) = \begin{cases}
    \{\emptyset\} & \text{if $k=0$} \\
   \emptyset & \text{if $s=\epsilon$} \\
    g(k,s,p) \cup f(k,tail(s),p+1) & \text{otherwise} \\
  \end{cases}
\end{equation}

\eqn{f-ksp} defines the function $f$ recursively.
The base case is when $k=0$, there is only one way to draw nothing from $s$,
that is the empty set. So $f(0,s,p)=\{\emptyset\}$.
Also when $s$ is an empty string ($s=\epsilon$), there is nothing to draw so
$f(k,\epsilon,p)=\emptyset.$
Finally, it reduces to two subproblems. One is to restrict the first segment
to start exactly at position $p$, the other one is to try a shorter string.
%
\begin{equation}
  \label{eqn:g-ksp}
  g(k,s,p) = \begin{cases}
    \{\emptyset\} & \text{if $k=0$} \\
   \emptyset & \text{if $s=\epsilon$} \\
    \{ g'(k,p,l,r) | \forall\langle l,r\rangle\in pairs(s) \} & \text{otherwise} \\
  \end{cases}
\end{equation}

\eqn{g-ksp} defines the function $g$ recursively.
The base case is when $k=0$, there is only one way to draw nothing from $s$,
that is the empty set. So $g(0,s,p)=\{\emptyset\}$.
Also when $s$ is an empty string ($s=\epsilon$), there is nothing to draw so
$g(k,\epsilon,p)=\emptyset.$
Finally, we use $pairs(s)$ to separate $s$ into two parts $\langle l,r\rangle$
such that $s=l::r$ in all the possible ways.
For each separated pair $\langle l,r\rangle$, we use $l$ as a selected segment,
and recursively call $f$ on $r$ to select the other $k-1$ segments.
%
\begin{equation}
  \label{eqn:g-kplr}
  g'(k,p,l,r) = \{ x\cup\{\langle l,p\rangle\} | \forall x\in f(k-1,r,p+length(l)) \}
\end{equation}

\eqn{g-kplr} shows the auxiliary function $g'$ which calls $f$ on string $r$
to generate all the combinations of $k-1$ disjoint segments.
%
\begin{equation}
  \label{eqn:tail-s}
  tail(s) = \begin{cases}
    xs & \text{if $s=x::xs$} \\
    \text{undefined} & \text{if $s=\epsilon$} \\
  \end{cases}
\end{equation}

\eqn{tail-s} defines $tail(s)$,
which is the remaining substring of $s$ excluding the first character.
A string is essentially a list of characters.  Here we use the notation $x::xs$
to denote a list with head $x$ and tail $xs$.
%
\begin{equation}
  \label{eqn:length-s}
  length(s) = \begin{cases}
    0 & \text{if $s=\epsilon$} \\
    1+length(tail(s)) & \text{otherwise} \\
  \end{cases}
\end{equation}

\eqn{length-s} defines $length(s)$,
which calculates the length of a string.  The empty string $\epsilon$
corresponds to the empty list, which has length 0.
The length of a non-empty string $s$ is the length of the tail of $s$ plus $1$.
%
\begin{equation}
  \label{eqn:pairs-s}
  pairs(s) = \begin{cases}
    \emptyset & \text{if $s=\epsilon$} \\
    \{\langle x::\epsilon,xs\rangle\}\cup\{\langle x::l,r\rangle|\forall\langle l,r\rangle\in pairs(xs)\} & \text{if $s=x::xs$} \\
  \end{cases}
\end{equation}

\eqn{pairs-s} defines $pairs(s)$,
which separates a string into two parts in all possible ways.
By using this definition, the left part is non-empty while
the right part may be empty.
For example, $pairs(``hello")$ is shown as follows.
\begin{align*}
 \langle ``h" &, ``ello" \rangle \\
 \langle ``he" &, ``llo" \rangle \\
 \langle ``hel" &, ``lo" \rangle \\
 \langle ``hell" &, ``o" \rangle \\
 \langle ``hello" &, \epsilon \rangle \\
\end{align*}

% TODO example for an output substring with many possible input substrings

\section{Position conjectures for input examples}
\label{sec:poconjs}

After we get the correspondences between the input and the output,
we can get a list of possibly useful positions in the input.
For each correspondence $\langle\vec{x},\hat{y},f\rangle$,
$\vec{x}$ is used to generate these positions.
A substring $s$ at position $p$ (denoted $\langle s,p\rangle$)
generates two positions $\{p,p+length(s)\}$.
From all substrings in all correspondences, we get the set of
possibly useful positions (indexes), denoted $\hat{P}$.
In later discussions, we use ``position" and ``index" interchangeably.

For each possibly useful index $i\in\hat{P}$ of the input $x$,
we divide $x$ into two parts, $x[0,i]$ and $x[i,length(x)]$.
Each character in either part is \emph{generalized} according to a
predefined \emph{character generalization hierarchy}.

A character generalization hierarchy (CGH) on input alphabet $\Sigma$
is a 5-tuple $$\langle G,generalize,similarity,merge,match\rangle$$
where
\begin{description}
  \item[$G$] is the generalized alphabet of $\Sigma$
  \item[$generalize:\Sigma\to G$] maps (generalizes) a symbol in $\Sigma$ to $G$
  \item[$similarity:G\times G\to\mathbb{R}$] is the similarity score between two generalized characters
  \item[$merge:G\times G\to G$] merges two generalized characters into a more general class
  \item[$match:G\times\Sigma\to\mathbb{B}$] decides whether a generalized character matches a input symbol
\end{description}
Generalized characaters are like token classes.  They include and can match the input symbols belonging to
that token class.  They also form a hierarchy so that two generalized characters can be merged together
to get a more general token class.
Next we describe two character generalization hierarchies.
The first one is an introduction to the basic notations.
The second one is a hierarchy in a more realistic setting.

\begin{example}
\label{ex-simple-cgh}
Assume there is an input alphabet $\Sigma$.
We define a character generalization hierarchy such that
\begin{itemize}
  \item $G=\Sigma\cup\{\top\}$ where $\top$ means \emph{any} character
  \item $similarity(x,y)=\begin{cases}
      1.0 & \text{if $x=y$} \\
      0.5 & \text{if $x\ne y$ and $x=\top\lor y=\top$} \\
      0.0 & \text{otherwise} \\
    \end{cases}$
  \item $merge(x,y)=\begin{cases}
      x & \text{if $x=y$} \\
   \top & \text{otherwise} \\
    \end{cases}$
  \item $match(g,c)=\begin{cases}
      \mathbf{true} & \text{if $g=c$ or $g=\top$} \\
     \mathbf{false} & \text{otherwise} \\
    \end{cases}$
\end{itemize}
This character generalization hierarchy is depicted as follows.
\begin{center}
  \begin{tikzpicture}
    \node (top) {$\top$};
    \node (dots1) [below=of top] {$\cdots$};
    \node (c) [left=of dots1] {$c$};
    \node (b) [left=of c] {$b$};
    \node (a) [left=of b] {$a$};
    \node (1) [right=of dots1] {$1$};
    \node (2) [right=of 1] {$2$};
    \node (3) [right=of 2] {$3$};
    \node (dots2) [right=of 3] {$\cdots$};
    \draw [->] (a) -- (top);
    \draw [->] (b) -- (top);
    \draw [->] (c) -- (top);
    \draw [->] (1) -- (top);
    \draw [->] (2) -- (top);
    \draw [->] (3) -- (top);
  \end{tikzpicture}
\end{center}
\end{example}

\begin{example}
In this example, the structure of the generalization hierarchy is shown in \fig{real-cgh}.
\begin{sidewaysfigure}
  \centering
  \begin{tikzpicture}[
      level/.style={sibling distance = 7cm/#1,level distance = 2cm}
    ]
    \node {any}
      child {
        node {alphanumeric}
        child {
          node {letter}
          child {
            node {uppercase}
            child { node {A..Z} }
          }
          child {
            node {lowercase}
            child { node {a..z} }
          }
        }
        child {
          node {digit}
          child { node {0..9} }
        }
      }
      child {
        node {punctuation}
        child {
          node {separator}
          child { node {...} }
        }
        child {
          node {operator}
          child { node {$+ - * / ...$} }
        }
        child {
          node {other}
          child { node {...} }
        }
      }
      child { node {whitespace} };
  \end{tikzpicture}
  \caption{A realistic character generalization hierarchy}
  \label{fig:real-cgh}
\end{sidewaysfigure}
The top of the hierarchy is the any character just as the one in Example \ref{ex-simple-cgh}.
The level below the top includes alphanumeric, punctuation, and whitespace characters.
For alphanumeric characters, there are letters and digits, where letters are further
decomposed into uppercase letters and lowercase letters.
The hierarchy under punctuation characters is rather subjective and just for demonstrational purpose.

Assume $\Sigma$ is the alphabet of ASCII characters.
The symbol set $G$ for the illustrated hierarchy is
$$ G=\Sigma\cup\{any,alphanumeric,punctuation,whitespace,\dots\} $$

Given this visualization of the hierarchy, we can further design the $similarity$ function
according to specific usage scenarios and contexts.
As for merging two generalized characters, we can find the least common ancestor (LCA)
of the two corresponding nodes in this tree as the $merge$ function.
The $match$ function is obvious given this hierarchy.
\end{example}

Character generalization hierarchies allow us to measure the similarity between
two conjectured positions.  This similarity is used along with the most similar
subsequence algorithm (described later) to help to find a consistent position conjecture.

\section{Most similar subsequences}
\label{sec:mss}

Given a quadruple $T=\langle \Sigma,\Gamma,\mathcal{S},\preccurlyeq\rangle$ where
\begin{description}
  \item[$\Sigma$] is the input alphabet,
  \item[$\Gamma$] is the output alphabet,
  \item[$\mathcal{S}:\Sigma\times\Gamma\to\mathbb{R}$] is a similarity function between an input symbol and an output symbol, and
  \item[$\preccurlyeq\subseteq \Sigma^*\times\Gamma^*$] is a binary relation such that $s\preccurlyeq t$ denotes $s$ is a subsequence\footnote{A subsequence is a sequence that can be derived from another sequence by deleting some elements without changing the order of the remaining elements.} of $t$,
\end{description}
the most similar subsequence (MSS) of $X\in\Sigma^*$ in $Y\in\Gamma^*,$ denoted $mss(X,Y),$
is a sequence $z\in\Gamma^{|X|}$ such that $z\preccurlyeq Y$ and
$\forall w\in\Gamma^{|X|}\left(w\preccurlyeq Y\to\mathcal{S}(X,w)\le\mathcal{S}(X,z)\right),$
where $|a_1 a_2\cdots a_n|=n$ is the length of a sequence.
We abuse $\mathcal{S}$ for the similarity between two sequences with equal length:
\begin{equation}
  \mathcal{S}(a_1 a_2\cdots a_n,b_1 b_2\cdots b_n)=\sum_{i=1}^n\mathcal{S}(a_i,b_i)
\end{equation}
%
By definition, there may be multiple MSS'es.

\subsection{Characterizing a most similar subsequence}

In a brute-force approach to solving the MSS problem, we would enumerate all subsequences of 
length $|X|$ of sequence $Y$ and check each subsequence to keep track of the most similar
subsequence we find.
This approach requires exponential time, making it impractical for long sequences.

% TODO optimal substructure

\subsection{A recursive solution}

An MSS of $X=x_1 x_2\cdots x_m$ in $Y=y_1 y_2\cdots y_n (m\le n)$ can be calculated by using 
dynamic programming techniques in polynomial time.
Let $c[i,j]$ be the length of an MSS of $X_i\in\Sigma^*$ in $Y_j\in\Gamma^*$ where
$X_i=x_1 x_2\cdots x_i$ and $Y_j=y_1 y_2\cdots y_j.$
The optimal substructure of the MSS problem gives the recursive formula
%
\begin{equation}
  \label{eqn:mss-length}
  c[i,j] = \begin{cases}
    0 & \text{if}~i=0 \\
    \max(c[i,j-1],c[i-1,j-1]+\mathcal{S}(x_i,y_j)) & \text{if}~j\ge i\ge 1 \\
  \end{cases}
\end{equation}

\subsection{Computing the length of an MSS}

Based on \eqn{mss-length}, we could easily write a recursive algorithm to compute 
the length of an MSS of two sequences in exponential time.  However, since the MSS 
problem has only $\Theta(mn)$ distinct subproblems, we can use dynamic programming 
to compute the solutions bottom up.

Procedure \textsc{MSS-Length} takes a similarity function $\mathcal{S}$ and
two sequences $X=x_1 x_2\cdots x_m$ and 
$Y=y_1 y_2\cdots y_n$ as inputs.  $c[i,j]$ values are stored in a table 
$c[0..m,0..n],$ whose entries are computed in row-major order.
(That is, the procedure fills in the first row of $c$ from left to right,
then the second row, and so on.)
The procedure also maintains the table $b[1..m,1..n]$ for the construction of an 
optimal solution.  Intuitively, $b[i,j]$ points to the cell corresponding
to the optimal solution of a subproblem, which is chosen when computing $c[i,j].$ 
The procedure returns the tables $b$ and $c$, where $c[m,n]$ contains
the length of an MSS of $X$ and $Y.$

\begin{algorithm}\rm\quad\\
\label{alg:mss-length}
$\textsc{MSS-Length}(\mathcal{S},X,Y)$
\begin{algorithmic}[1]
  \State $m=X.length$
  \State $n=Y.length$
  \State let $b[1..m,1..n]$ and $c[0..m,0..n]$ be new tables
  \For {$j=0\To n-m$}
    \State $c[0,j]=0$
  \EndFor
  \For {$i=1\To m$}
    \State $c[i,i] = c[i-1,i-1] + \mathcal{S}(x_i,y_i)$
    \State $b[i,i] = ``\nwarrow"$
    \For {$j=i+1\To i+n-m$}
      \If {$c[i,j-1]\ge c[i-1,j-1]+\mathcal{S}(x_i,y_j)$}
        \State $c[i,j]=c[i,j-1]$
        \State $b[i,j]=``\leftarrow"$
      \Else
        \State $c[i,j]=c[i-1,j-1]+\mathcal{S}(x_i,y_j)$
        \State $b[i,j]=``\nwarrow"$
      \EndIf
    \EndFor
  \EndFor
\end{algorithmic}
\end{algorithm}

\begin{figure}
\centering
\begin{tikzpicture}[cell/.style={rectangle,draw=black},
                    nocell/.style={draw=none,fill=none},
                    space/.style={minimum height=2.5em,
                                  matrix of nodes,
                                  row sep=-\pgflinewidth,
                                  column sep=-\pgflinewidth,
                                  minimum width=2.5em},
                    text depth=0.5ex,
                    text height=2ex,
                    nodes in empty cells]
\matrix[space,
column 3/.style={nodes=cell},
column 4/.style={nodes=cell},
column 5/.style={nodes=cell},
column 6/.style={nodes=cell},
column 7/.style={nodes=cell},
column 8/.style={nodes=cell},
column 9/.style={nodes=cell},
column 10/.style={nodes=cell},
row 1/.style={nodes=nocell},
row 2/.style={nodes=nocell}]
{
  \   & $j$   & $0$   & $1$ & $2$ & $3$ & $4$ & $5$ & $6$ & $7$ \\
  $i$ &       & $y_j$ & $A$ & $B$ & $C$ & $B$ & $D$ & $A$ & $B$ \\
  $0$ & $x_i$ & $0$   & $0$ &  &  &  &  &  &  \\
  $1$ & $B$   &       &|[fill=yellow]| $\nwarrow0$ & $\nwarrow1$ &     &     &     &     &     \\
  $2$ & $D$   &       &     &|[fill=yellow]| $\nwarrow0$ & $\nwarrow1$ &     &     &     &     \\
  $3$ & $C$   &       &     &     &|[fill=yellow]| $\nwarrow1$ & $\leftarrow1$ &     &     &     \\
  $4$ & $A$   &       &     &     &     &|[fill=yellow]| $\nwarrow1$ & $\leftarrow1$ &     &     \\
  $5$ & $B$   &       &     &     &     &     &|[fill=yellow]| $\nwarrow1$ & $\leftarrow1$ &     \\
  $6$ & $A$   &       &     &     &     &     &     &|[fill=yellow]| $\nwarrow2$ &|[fill=yellow]| $\leftarrow2$ \\
};
\end{tikzpicture}
\caption{\rm
The $c$ and $b$ tables computed by \textsc{MSS-Length} on the sequences 
$X=\langle B,D,C,A,B,A\rangle$ and $Y=\langle A,B,C,B,D,A,B\rangle.$
The square in row $i$ and column $j$ contains the value of $c[i,j]$
and the corresponding arrow for the value of $b[i,j].$ The number $2$
in $c[6,7]$ is the length of an MSS of $X$ in $Y.$  For $j\ge i\ge 1,$
$c[i,j]$ depends only on the values of $c[i-1,j-1]$ and $c[i-1,j].$
To reconstruct the elements of an MSS, follow the $b[i,j]$ arrows
from the lower right-hand corner; the sequence is highlighted.
Each $``\nwarrow"$ on the highlighted sequence corresponds to 
a member of the MSS.}
\label{fig:mss-example}
\end{figure}

\fig{mss-example} shows the tables produced by \textsc{MSS-Length} on the sequences
$X=\langle B,D,C,A,B,A\rangle$ and $Y=\langle A,B,C,B,D,A,B\rangle.$
The similarity function $\mathcal{S}$ is defined as
\begin{equation}
  \mathcal{S}(a,b) = \begin{cases}
    1.0 & \text{if $a=b$} \\
    0.0 & \text{otherwise}
  \end{cases}
\end{equation}
The running time of the procedure is $\Theta(mn),$ since each cell in the table
takes $\Theta(1)$ time to compute.

\subsection{Constructing an MSS}

The $b$ table returned by \textsc{MSS-Length} enables us to quickly construct an MSS
of $X=x_1 x_2\cdots x_m$ and $Y=y_1 y_2\cdots y_n.$  We can simply start from $b[m,n]$
and trace through the table by following the arrows.  Whenever a $``\nwarrow"$ is 
encountered in a cell $b[i,j]$, it implies that $y_j$ is an element of the MSS that
\textsc{MSS-Length} found.  With this method, the elements of this MSS are encountered
in reverse order.  The recursive procedure described below prints out an MSS of $X$ in $Y$
in the proper, forward order.  The initial call is $\textsc{Print-MSS}(b,X,X.length,Y.length).$

\begin{algorithm}\rm\quad\\
\label{alg:print-mss}
$\textsc{Print-MSS}(b,X,i,j)$
\begin{algorithmic}[1]
  \If {$i>0$ and $j>0$}
    \If {$b[i,j]=``\nwarrow"$}
      \State $\textsc{Print-MSS}(b,X,i-1,j-1)$
      \State print $y_j$
    \Else
      \State $\textsc{Print-MSS}(b,X,i,j-1)$
    \EndIf
  \EndIf
\end{algorithmic}
\end{algorithm}

For the $b$ table in \fig{mss-example}, this procedure prints $ABCBDA.$
The procedure takes time $O(m+n),$ since it decrements at least one of 
$i$ and $j$ in each recursive call.

\section{A consistent position conjecture}

In Section \ref{sec:poconjs}, we compute a list of position conjectures
for each input example.  A position conjecture $poconj\in G^*\times G^*$
is a pair of generalized character strings.
Now we want to get a list of position conjectures which is consistent
with all the input examples.

In Nix's EBE system \cite[1]{nix1984editing}, positions are located
by using the longest common subsequence of all the input examples.
Computing the LCS of $n>2$ strings is NP-hard in general, so
an approximation is employed that
\begin{equation}
  LCS(x_1,x_2,\cdots,x_n) \approx LCS(x_n,\cdots,LCS(x_3, LCS(x_2, x_1))\cdots)
\end{equation}
Without loss of generality, $x_1,x_2,\cdots,x_n$ are sorted by length before this approximation.

In our system, positions are located by using the conjectures of
generalized characters.  Generalized characters differ from plain characters
in a LCS in that they have similarities.  Similarities are much more general
than plain equality.  However, this also demands a more general algorithm
to find the most similar subsequence (MSS), which is described in Section \ref{sec:mss}.

In our definition of $MSS(x,y)$, the result comes only from $y$, while in LCS,
the result is actually a combined subsequence.  So we also define a combine function
$COM$ for two sequences of position conjectures.  The final consistent position conjecture
is calculated by following the steps below.
\begin{align}
  t_1 &= x_1 \\
  t_2 &= COM(t_1,MSS(t_1,x_2)) \\
  t_3 &= COM(t_2,MSS(t_2,x_3)) \\
      &\cdots \\
  t_n &= COM(t_{n-1},MSS(t_{n-1},x_n))
\end{align}

The $COM$ function is defined by using the $merge$ function in a character generalization
hierarchy.
\begin{align}
  COM(\epsilon,\epsilon) &= \epsilon \\
        COM(x::xs,y::ys) &= COM'(x,y) :: COM(xs,ys) \\
  COM'(\langle l_1,r_1\rangle,\langle l_2,r_2\rangle) &= \langle rev(COM''(rev(l_1),rev(l_2))), COM''(r_1,r_2)\rangle \\
                             COM''(\epsilon,ys) &= \epsilon \\
                             COM''(xs,\epsilon) &= \epsilon \\
                                   COM''(x::xs,y::ys) &= merge(x,y) :: COM''(xs,ys) \\
                                        rev(\epsilon) &= \epsilon \\
                                           rev(x::xs) &= rev(xs)::x::\epsilon
\end{align}

Then we discuss the measurement of similarities between position conjectures.
Let $SIM:(G^*\times G^*)\times(G^*\times G^*)\to\mathbb{R}$ denote the similarity function
between two position conjectures.  We define $SIM$ as follows.
\begin{align}
  SIM(\langle l_1,r_1\rangle,\langle l_2,r_2\rangle) &= SIM'(rev(l_1),rev(l_2)) + SIM'(r_1,r_2) \\
                                   SIM'(\epsilon,ys) &= 0.0 \\
                                   SIM'(xs,\epsilon) &= 0.0 \\
                                   SIM'(x::xs,y::ys) &= similarity(x,y) + SIM'(xs,ys)
\end{align}

%\section{Disambiguating position conjectures}

%\section{Building the parser}

\section{Input parse results}
We use the synthesized parser to parse all the input examples.
Each input example is parsed into a list of disjoint substrings such that
these substrings can be concatenated to get the corresponding input.

\section{Output conjectures}
An output conjecture for an input-output example is a nondeterministic
finite-state automaton which accepts the output string.
Transitions of the automaton include constant strings as well as
transformation functions.
The construction of the automaton is trivial.
The following example illustrates the basic idea of an output conjecture.

\begin{example}
In this example, we have access to a binary function $add(x,y)=x+y$ for adding two
integers together.  The user provides an example as follows.
\begin{center}
\begin{minipage}{0.85\textwidth}
\begin{verbatim}
0                        0                   1
0 1 2 3 4 5 6 7 8 9      0 1 2 3 4 5 6 7 8 9 0 1
 1 2 3 + 7 8 9 = ?   =>   1 2 3 + 7 8 9 = 9 1 2
\end{verbatim}
\end{minipage}
\end{center}
The correspondences are listed below. $x$ and $y$ denote the input and the output, respectively.
\begin{center}\rm
  \begin{tabular}{lll}
    \hline
    Input substrings & Output substring & Transformation function \\
    \hline
    $x[0,8]$ & $y[0,8]$ & indentity function \\
    $x[0,3],x[4,7]$ & $y[8,11]$ & $add$ \\
    \hline
  \end{tabular}
\end{center}
The input $x$ is then parsed as follows.
\begin{center}\rm
  \begin{tabular}{ll}
    \hline
    Register & Content \\
    \hline
    a & \verb!123! \\
    b & \verb!+! \\
    c & \verb!789! \\
    d & \verb!=! \\
    e & \verb!?! \\
    \hline
  \end{tabular}
\end{center}
The synthesized output conjecture is as follows.
\begin{center}
  \includegraphics[width=\textwidth]{figures/output-conjecture}
\end{center}
This automaton essentially contains all possible ways to generate the output.
\end{example}

\section{Automata intersection}
From each input-output example we get a output conjecture automaton.
The next step is to intersect all these automata to get a consistent automaton.

There is a systematic way for intersection of automata.
Let $A$ and $B$ be the input automata.
The states of new automaton will be all pairs of states of $A$ and $B$,
that is $S_{A\cap B}=S_A\times S_B$,
the initial state will be $i_{A\cap B}=\langle i_A,i_B\rangle$,
where $i_A$ and $i_B$ are the initial states of $A$ and $B$,
and $F_{A\cap B}=F_A\times F_B$ where $F_X$ denotes the set of accepting states of $X$.
Finally, the transition function $\delta_{A\cap B}$ is defined as follows
for any letter $\alpha\in\Sigma$ and states $p_1,p_2\in S_A, q_1,q_2\in S_B$:
\begin{equation}
  \langle p_1, q_1 \rangle
  \xrightarrow[A \cap B]{\ \alpha\ }
  \langle p_2, q_2 \rangle
  \quad
  \text{ iff }
  \quad
  p_1 \xrightarrow[A]{\ \alpha\ } p_2
  \quad
  \text{and}
  \quad
  q_1 \xrightarrow[B]{\ \alpha\ } q_2
\end{equation}

Note that such automaton usually is not minimal (e.g. the intersection might be just an empty language).
Also it might be useful (but it is not necessary) to make input automata minimal since the output is quadratic in size.
Automaton minimization is indeed employed in our approach.

\section{Output transducer disambiguation}
In case that only one example is provided, it likely that the consistent output conjecture is ambiguous.
Even multiple examples are provided, the intersection result may still be ambiguous.
So the intersected consistent output conjecture demands disambiguation.
In our approach, we propose a disambiguation method based on finding the \emph{shortest path} in the synthesized
output conjecture.
The following example illustrates this disambiguation method.

\begin{example}
  Suppose the consistent output conjecture is as follows.
  \begin{center}
    \includegraphics[width=\textwidth]{figures/output-conjecture}
  \end{center}
  In most cases, we want to find the minimal description of the user intent.
  In the context of output conjecture, the minimality corresponds to the shortest path from the initial state
  to the final accepting state.
  So we need to assign weights/costs to the transitions (the edges).

  In this example, constant transitions have weight $1.0$ while other transitions have weight $2.0$.
  These weights are selected for the sake of simplicity, but also represent the minimality to some extent.
  Then the shortest path is the one highlighted below, which has the cost of $8.0$.  It is obvious that this heuristic helps a lot.
  \begin{center}
    \includegraphics[width=\textwidth]{figures/outconj-disambiguation}
  \end{center}
\end{example}

