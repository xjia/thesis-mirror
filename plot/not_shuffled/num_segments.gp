reset
unset log
unset label
set key right bottom
#set xtics (10,20,30,40)
set xlabel "Number of Segments"
set ylabel "Precision"
set terminal postscript eps enhanced color "Helvetica" 25
set out "n_num_segments.precision.eps"
plot "not_shuffled.dat" using 4:5 title "" with points, \
     "num_segments.dat" using 1:3 title "Average" with lines, \
     "num_segments.dat" using 1:2 title "Min" with lines, \
     "num_segments.dat" using 1:4 title "Max" with lines

reset
unset log
unset label
set key right top
#set xtics (10,20,30,40)
set xlabel "Number of Segments"
set ylabel "Recall"
set terminal postscript eps enhanced color "Helvetica" 25
set out "n_num_segments.recall.eps"
plot "not_shuffled.dat" using 4:6 title "" with points, \
     "num_segments.dat" using 1:6 title "Average" with lines, \
     "num_segments.dat" using 1:5 title "Min" with lines, \
     "num_segments.dat" using 1:7 title "Max" with lines

reset
unset log
unset label
set key right top
#set xtics (10,20,30,40)
set xlabel "Number of Segments"
set ylabel "F1"
set terminal postscript eps enhanced color "Helvetica" 25
set out "n_num_segments.f1.eps"
plot "not_shuffled.dat" using 4:7 title "" with points, \
     "num_segments.dat" using 1:9 title "Average" with lines, \
     "num_segments.dat" using 1:8 title "Min" with lines, \
     "num_segments.dat" using 1:10 title "Max" with lines

reset
unset log
unset label
set key right top
#set xtics (10,20,30,40)
set xlabel "Number of Segments"
set ylabel "Time (sec)"
set terminal postscript eps enhanced color "Helvetica" 25
set out "n_num_segments.time.eps"
plot "not_shuffled.dat" using 4:8 title "" with points, \
     "num_segments.dat" using 1:12 title "Average" with lines, \
     "num_segments.dat" using 1:11 title "Min" with lines, \
     "num_segments.dat" using 1:13 title "Max" with lines

