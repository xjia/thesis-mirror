reset
unset log
unset label
set key left bottom
#set xtics (10,20,30,40)
set xlabel "Noise Ratio"
set ylabel "Precision"
set terminal postscript eps enhanced color "Helvetica" 25
set out "n_noise_ratio.precision.eps"
plot "not_shuffled.dat" using 3:5 title "" with points, \
     "noise_ratio.dat" using 1:3 title "Average" with lines, \
     "noise_ratio.dat" using 1:2 title "Min" with lines, \
     "noise_ratio.dat" using 1:4 title "Max" with lines

reset
unset log
unset label
set key left bottom
#set xtics (10,20,30,40)
set xlabel "Noise Ratio"
set ylabel "Recall"
set terminal postscript eps enhanced color "Helvetica" 25
set out "n_noise_ratio.recall.eps"
plot "not_shuffled.dat" using 3:6 title "" with points, \
     "noise_ratio.dat" using 1:6 title "Average" with lines, \
     "noise_ratio.dat" using 1:5 title "Min" with lines, \
     "noise_ratio.dat" using 1:7 title "Max" with lines

reset
unset log
unset label
set key left bottom
#set xtics (10,20,30,40)
set xlabel "Noise Ratio"
set ylabel "F1"
set terminal postscript eps enhanced color "Helvetica" 25
set out "n_noise_ratio.f1.eps"
plot "not_shuffled.dat" using 3:7 title "" with points, \
     "noise_ratio.dat" using 1:9 title "Average" with lines, \
     "noise_ratio.dat" using 1:8 title "Min" with lines, \
     "noise_ratio.dat" using 1:10 title "Max" with lines

reset
unset log
unset label
set key left top
#set xtics (10,20,30,40)
set xlabel "Noise Ratio"
set ylabel "Time (sec)"
set terminal postscript eps enhanced color "Helvetica" 25
set out "n_noise_ratio.time.eps"
plot "not_shuffled.dat" using 3:8 title "" with points, \
     "noise_ratio.dat" using 1:12 title "Average" with lines, \
     "noise_ratio.dat" using 1:11 title "Min" with lines, \
     "noise_ratio.dat" using 1:13 title "Max" with lines

