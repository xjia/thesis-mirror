reset
unset log
unset label
set key right bottom
#set xtics (10,20,30,40)
set xlabel "Alphabet Size"
set ylabel "Precision"
set terminal postscript eps enhanced color "Helvetica" 25
set out "r_alphabet_size_100-precision.eps"
plot "random_shuffled_100.dat" using 1:5 title "" with points, \
     "alphabet_size_100.dat" using 1:3 title "Average" with lines lw 8, \
     "alphabet_size_100.dat" using 1:2 title "Min" with lines lw 4, \
     "alphabet_size_100.dat" using 1:4 title "Max" with lines lw 4

reset
unset log
unset label
set key right bottom
#set xtics (10,20,30,40)
set xlabel "Alphabet Size"
set ylabel "Recall"
set terminal postscript eps enhanced color "Helvetica" 25
set out "r_alphabet_size_100-recall.eps"
plot "random_shuffled_100.dat" using 1:6 title "" with points, \
     "alphabet_size_100.dat" using 1:6 title "Average" with lines lw 8, \
     "alphabet_size_100.dat" using 1:5 title "Min" with lines lw 4, \
     "alphabet_size_100.dat" using 1:7 title "Max" with lines lw 4

reset
unset log
unset label
set key right bottom
#set xtics (10,20,30,40)
set xlabel "Alphabet Size"
set ylabel "F1"
set terminal postscript eps enhanced color "Helvetica" 25
set out "r_alphabet_size_100-f1.eps"
plot "random_shuffled_100.dat" using 1:7 title "" with points, \
     "alphabet_size_100.dat" using 1:9 title "Average" with lines lw 8, \
     "alphabet_size_100.dat" using 1:8 title "Min" with lines lw 4, \
     "alphabet_size_100.dat" using 1:10 title "Max" with lines lw 4

reset
unset log
unset label
set key right top
#set xtics (10,20,30,40)
set xlabel "Alphabet Size"
set ylabel "Time (sec)"
set terminal postscript eps enhanced color "Helvetica" 25
set out "r_alphabet_size_100-time.eps"
plot "random_shuffled_100.dat" using 1:8 title "" with points, \
     "alphabet_size_100.dat" using 1:12 title "Average" with lines lw 8, \
     "alphabet_size_100.dat" using 1:11 title "Min" with lines lw 4, \
     "alphabet_size_100.dat" using 1:13 title "Max" with lines lw 4

reset
unset log
unset label
set key right bottom
#set xtics (10,20,30,40)
set xlabel "Alphabet Size"
set ylabel "Precision"
set terminal postscript eps enhanced color "Helvetica" 25
set out "r_alphabet_size_1000-precision.eps"
plot "random_shuffled_1000.dat" using 1:5 title "" with points, \
     "alphabet_size_1000.dat" using 1:3 title "Average" with lines lw 8, \
     "alphabet_size_1000.dat" using 1:2 title "Min" with lines lw 4, \
     "alphabet_size_1000.dat" using 1:4 title "Max" with lines lw 4

reset
unset log
unset label
set key right bottom
#set xtics (10,20,30,40)
set xlabel "Alphabet Size"
set ylabel "Recall"
set terminal postscript eps enhanced color "Helvetica" 25
set out "r_alphabet_size_1000-recall.eps"
plot "random_shuffled_1000.dat" using 1:6 title "" with points, \
     "alphabet_size_1000.dat" using 1:6 title "Average" with lines lw 8, \
     "alphabet_size_1000.dat" using 1:5 title "Min" with lines lw 4, \
     "alphabet_size_1000.dat" using 1:7 title "Max" with lines lw 4

reset
unset log
unset label
set key right bottom
#set xtics (10,20,30,40)
set xlabel "Alphabet Size"
set ylabel "F1"
set terminal postscript eps enhanced color "Helvetica" 25
set out "r_alphabet_size_1000-f1.eps"
plot "random_shuffled_1000.dat" using 1:7 title "" with points, \
     "alphabet_size_1000.dat" using 1:9 title "Average" with lines lw 8, \
     "alphabet_size_1000.dat" using 1:8 title "Min" with lines lw 4, \
     "alphabet_size_1000.dat" using 1:10 title "Max" with lines lw 4

reset
unset log
unset label
set key right top
#set xtics (10,20,30,40)
set xlabel "Alphabet Size"
set ylabel "Time (sec)"
set terminal postscript eps enhanced color "Helvetica" 25
set out "r_alphabet_size_1000-time.eps"
plot "random_shuffled_1000.dat" using 1:8 title "" with points, \
     "alphabet_size_1000.dat" using 1:12 title "Average" with lines lw 8, \
     "alphabet_size_1000.dat" using 1:11 title "Min" with lines lw 4, \
     "alphabet_size_1000.dat" using 1:13 title "Max" with lines lw 4

