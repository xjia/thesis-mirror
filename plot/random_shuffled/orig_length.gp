reset
unset log
unset label
set key left bottom
#set xtics (10,20,30,40)
set xlabel "Original Length"
set ylabel "Precision"
set terminal postscript eps enhanced color "Helvetica" 25
set out "r_orig_length-precision.eps"
plot "random_shuffled.dat" using 2:5 title "" with points, \
     "orig_length.dat" using 1:3 title "Average" with lines lw 8, \
     "orig_length.dat" using 1:2 title "Min" with lines lw 4, \
     "orig_length.dat" using 1:4 title "Max" with lines lw 4

reset
unset log
unset label
set key left bottom
#set xtics (10,20,30,40)
set xlabel "Original Length"
set ylabel "Recall"
set terminal postscript eps enhanced color "Helvetica" 25
set out "r_orig_length-recall.eps"
plot "random_shuffled.dat" using 2:6 title "" with points, \
     "orig_length.dat" using 1:6 title "Average" with lines lw 8, \
     "orig_length.dat" using 1:5 title "Min" with lines lw 4, \
     "orig_length.dat" using 1:7 title "Max" with lines lw 4

reset
unset log
unset label
set key left bottom
#set xtics (10,20,30,40)
set xlabel "Original Length"
set ylabel "F1"
set terminal postscript eps enhanced color "Helvetica" 25
set out "r_orig_length-f1.eps"
plot "random_shuffled.dat" using 2:7 title "" with points, \
     "orig_length.dat" using 1:9 title "Average" with lines lw 8, \
     "orig_length.dat" using 1:8 title "Min" with lines lw 4, \
     "orig_length.dat" using 1:10 title "Max" with lines lw 4

reset
unset log
unset label
set key left top
#set xtics (10,20,30,40)
set xlabel "Original Length"
set ylabel "Time (sec)"
set terminal postscript eps enhanced color "Helvetica" 25
set out "r_orig_length-time.eps"
plot "random_shuffled.dat" using 2:8 title "" with points, \
     "orig_length.dat" using 1:12 title "Average" with lines lw 8, \
     "orig_length.dat" using 1:11 title "Min" with lines lw 4, \
     "orig_length.dat" using 1:13 title "Max" with lines lw 4

